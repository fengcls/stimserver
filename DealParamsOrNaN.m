function [varargout] = DealParamsOrNaN(varargin)

% DealParamsOrNaN - FUNCTION Deal out a list of parameters, or return nan
%
% Usage: [varargout] = DealParamsOrNaN(varargin)
%
% Use like [a, b, c, d] = DealParamsOrNaN(cellParamsList{:});
%
% a, b, c and d will be assigned values in order from 'cellParamsList'.  If
% 'cellParamsList' is too short, excess return parameters will be returned as
% NaNs.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 1st October, 2010

if (nargin < nargout)
   varargin(end+1:nargout) = {nan};
end

varargout = varargin;
