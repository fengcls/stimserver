function [strDescription] = DescribeBandLimitedNoiseStimulus(sStimulus, strIndent)

% BandLimitedNoise - STIMULUS A random stimulus, composed of spatially- and temporally-band-limited white noise
%
% This is a "lava lamp" stimulus, constructed by spatially and temporally
% filtering a white noise sequence with bandpass (actually low-pass) filters.
% 
% Presentation parameters:
%
%     tFrameDuration - The desired duration of a single frame
%     fPPD - The calibration of the display monitor (pixels per degree)
%     nNumRepeats - The number of times to repeat the presentation of the
%        single band-pass filtered noise stimulus
%
% Stimulus parameters:
%
%     vfSpatFreqCutoffCPD - The cut-off frequencies for the spatial low-pass
%        filter, in cycles per degree.  The arguments are ['fLowCutCPD'
%        'fHighCutCPD'].  If a scalar value is supplied, a low-pass filter will
%        be applied between 0 and the supplied value.
%
%     vfTempFreqCutoffHz - The cut-off frequencies for the temporal low-pass
%        filter, in Hertz (cycles per second).  The arguments are ['fLowCutHz'
%        'fHighCutHz'].  If a scalar value is supplied, a low-pass filter will
%        be applied between 0 and the supplied value.
%
%     vfStimSizeDeg - A two-element vector [fXSize fYSize], specifying the
%        desired size of the stimulus, in degrees.  If an empty matrix is
%        provided then the stimulus will fill the presentation window.
%
%     tStimDuration - The desired stimulus duration, in seconds.
%
%     fSpatSamplingSPD - The spatial sampling resolution used to generate the
%        stimulus, in samples per degree.  Basically, the white noise movie can
%        end up very large.  By reducing the spatial resolution of the movie we
%        can save memory.  When the movie is presented, it will be scaled up
%        using a bicubic filter by the graphics card.  Since the movie was
%        spatially filtered anyway, you can sub-sample the stimulus without any
%        loss of quality.
%
%     tWindowTime - An optional parameter specifying that the stimulus movie
%        should gradually increase in contrast from 0% to 100% at the onset of
%        presentation, and decrease gradually from 100% to 0% on stimulus
%        offset, with a duration of 'tWindowTime' seconds for each of the onset
%        and offset.  The default is 0 seconds, meaning that the stimulus begins
%        at 100% contrast.
%
%     tModulationPeriod - An optional parameter used to modulate the contrast of
%        the stimulus between zero and the maximum, using a sinusoidal profile
%        of modulation.  The period is the time for a full cycle between zero
%        contrast, maximum contrast and back to zero contrast.
%
%     fModulationAmplitude - An optional parameter used to modify the maximum
%        aparrent contrast of the stimulus.  By default it is set to "1", which
%        implies that the stimulus spans the full range between the darkest and
%        brightest pixel values possible.  Each frame of the stimulus is
%        guaranteed to span this range.  'fModulationAmplitude' is a
%        multiplication factor to this contrast range.  "0" implies zero
%        contrast; greater than 1 implies that the stimulus will be "stretched"
%        to span double the contrast range, but then be clipped to what the
%        screen can display.  Note that this will change the spatial frequency
%        content of the stimulus, but increase the perceived contrast of the
%        stimulus.
%
%     nSeed - A number used as a random seed, to ensure that each pseudo-random
%        stimulus is repeatable.

% DescribeBandLimitedNoiseStimulus - FUNCTION Describe the parameters for a band-limited white noise stimulus
%
% Usage: [strDescription] = DescribeBandLimitedNoiseStimulus(sStimulus <, strIndent>)
%
% Usually called automatically by StimulusServerDescribeStimuli

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 15ht October, 2010

% -- Check arguments

if (nargin == 0)
   SSlog('*** DescribeBandLimitedNoiseStimulus: Incorrect usage');
   help DescribeBandLimitedNoiseStimulus;
   return;
end

if (~exist('strIndent', 'var') || isempty(strIndent))
   strIndent = '';
end


% -- Convert parameters to parameter strings

% - Extract parameters from stimulus
[tFrameDuration, fPixelsPerDegree, nNumRepeats] = DealParamsOrNaN(sStimulus.cPresentationParameters{:});
[fSpatFreqCutoffCPD, fTempFreqCutoffHz, vfStimSizeDeg, tStimulusDuration, fSpatSamplingSPD, tWindowTime, nSeed] = ...
      DealParamsOrNaN(sStimulus.cStimulusArguments{:});

strFrameDuration = CheckNanSprintf('%.2f ms', [], tFrameDuration*1e3, tFrameDuration*1e3);
strPixelsPerDegree = CheckNanSprintf('%.2f ppd', [], fPixelsPerDegree, fPixelsPerDegree);
strNumRepeats = CheckNanSprintf('%d', [], nNumRepeats, nNumRepeats);
strSpatFreqCutoffCPD = CheckNanSprintf('%.2f cpd', [], fSpatFreqCutoffCPD, fSpatFreqCutoffCPD);
strTempFreqCutoffHz = CheckNanSprintf('%.2f Hz', [], fTempFreqCutoffHz, fTempFreqCutoffHz);
strSizeDegrees = CheckEmptyNanSprintf('[%.2f x %.2f] deg', '(full screen)', [], vfStimSizeDeg, vfStimSizeDeg);
strStimulusDuration = CheckNanSprintf('%.2f s', [], tStimulusDuration, tStimulusDuration);
strSpatSamplingSPD = CheckNanSprintf('%.2f samples per degree', [], fSpatSamplingSPD, fSpatSamplingSPD);
strWindowTime = CheckNanSprintf('%.2f ms', [], tWindowTime*1e3, tWindowTime*1e3);
strSeed = CheckEmptyNanSprintf('%d', '(1)', [], nSeed, nSeed);

   
% -- Produce description
   
strDescription = [strIndent sprintf('Oscillating grating stimulus\n') ...
                  strIndent sprintf('Parameter list:\n')];
strIndent = [strIndent '   '];

strDescription = [strDescription ...
   strIndent sprintf('Frame duration: %s\n', strFrameDuration) ...
   strIndent sprintf('Screen calibration: %s\n', strPixelsPerDegree) ...
   strIndent sprintf('Number of repeats: %s\n', strNumRepeats) ...
   strIndent sprintf('Spatial frequency cut-off: %s\n', strSpatFreqCutoffCPD) ...
   strIndent sprintf('Temporal frequency cut-off: %s\n', strTempFreqCutoffHz) ...
   strIndent sprintf('Stimulus size: %s\n', strSizeDegrees) ...
   strIndent sprintf('Stimulus duration: %s\n', strStimulusDuration) ...
   strIndent sprintf('Spatial sampling resolution: %s\n', strSpatSamplingSPD) ...
   strIndent sprintf('Windowing time: %s\n', strWindowTime) ...
   strIndent sprintf('Random seed: %s\n', strSeed)];

% --- END of DescribeBandLimitedNoiseStimulus ---


function strString = CheckNanSprintf(strFormat, strEmptyFormat, oCheckNan, varargin)

if (~isnan(oCheckNan))
   strString = sprintf(strFormat, varargin{:});

elseif isempty(strEmptyFormat)
   strString = '(dynamic)';

else
   strString = sprintf(strEmptyFormat, varargin{:});
end


function strString = CheckEmptyNanSprintf(strFormat, strEmptyFormat, strNanFormat, oCheckEmptyNan, varargin)

if (isnan(oCheckEmptyNan))
   if (isempty(strNanFormat))
      strString = '(dynamic)';
   else
      strString = sprintf(strNanFormat, varargin{:});
   end
   
elseif (isempty(oCheckEmptyNan))
   if isempty(strEmptyFormat)
      strString = '(dynamic)';
   else
      strString = sprintf(strEmptyFormat, varargin{:});
   end
   
else
   strString = sprintf(strFormat, varargin{:});
end

% --- END of DescribeBandLimitedNoiseStimulus.m ---
