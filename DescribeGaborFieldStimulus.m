function [strDescription] = DescribeGaborFieldStimulus(sStimulus, strIndent)

% GaborFieldStimulus - STIMULUS An arbitrary field of Gabors
%
% This stimulus generates a field of an arbitrary number of Gabor patches, with
% arbitrary locations and independent parameters.
% 
% Presentation parameters:
%
%     tFrameDuration - The desired frame duration, in seconds.  Use
%        'GetSafeFrameDurations' to get a value for this parameter.
%
%     tStimulusDuration - The desired duration of the entire stimulus, in
%        seconds.
%
% Stimulus parameters: 
%
%     mfGaborLocsDeg - A matrix [Nx2], defining the locations of each of N Gabor
%        patches.  Each row defines the location [fXOffsetDeg fYOffsetDeg], as
%        offsets from the centre of the stimulus window, in degrees.  NOTE: If
%        you want a regular grid, see STIM_GaborGrid.
%
%     fPixelsPerDeg - The calibration of the stimulus presentation screen, in
%        pixels per degree.
%
%     vfGaborWidthDeg - The width of each Gabor patch, in degrees.  The width is
%        taken as two standard deviations of the Gaussian used to mask the
%        Gabor.  Either a scalar value can be supplied, or a vector of widths,
%        one for each Gabor patch.
%
%     vfGaborSFCPD - The spatial frequency of each Gabor patch, in cycles per
%        degree.  Either a scalar value can be supplied, or a vector of spatial
%        frequencies, one for each Gabor patch.
%
%     vfGaborContrast - The contrast of each Gabor patch.  '1' (default) scales
%        the underlying grating such that it spans minimum to maximum luminance
%        on the stimulus screen.  Values greater than one will clip; values less
%        than one will have reduced contrast for each patch.  Either a scalar
%        value can be supplied, or a vector of contrasts, one for each Gabor
%        patch.
%
%     vfGaborOrientationRad - The orientation (and drift direction) of each
%        Gabor patch, in radians. '0' (default) leads to vertical bars, drifting
%        to the left.  'pi/2' leads to horizontal bars, drifting upwards.
%        Either a scalar value can be supplied, or a vector of orientations, one
%        for each Gabor patch.
%
%     vfGaborPhaseRad - The initial phase shift of each Gabor patch, in radians.
%        '0' (default) leads to an odd-phase Gabor patch.  Increasing phases
%        shift the underlying grating in the direction of drift (see above).
%        Either a scalar value can be supplied, or a vector of phases, one
%        for each Gabor patch.
%
%     vfGaborDriftTFHz - The phase drift rate of each Gabor patch, in cycles per
%        second.  By default, Gabors do not drift ('0'). Either a scalar value
%        can be supplied, or a vector of drift rates, one for each Gabor patch.
%
%     vfGaborRotationHz - The rotation rate of each Gabor patch, in cycles per
%        second.  By default, Gabors do not rotate ('0'). Either a scalar value
%        can be supplied, or a vector of rotation rates, one for each Gabor
%        patch.

% DescribeGaborFieldStimulus - FUNCTION Describe the parameters for a "GaborField" stimulus
%
% Usage: [strDescription] = DescribeGaborFieldStimulus(sStimulus <, strIndent>)
%
% Usually called automatically by StimulusServerDescribeStimuli

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 9th November, 2012

% -- Check arguments

if (nargin == 0)
   SSlog('*** DescribeGaborFieldStimulus: Incorrect usage\n');
   disp('Usage: [strDescription] = DescribeGaborFieldStimulus(sStimulus <, strIndent>);');
   return;
end

if (~exist('strIndent', 'var') || isempty(strIndent))
   strIndent = '';
end


% -- Convert parameters to parameter strings

% - Extract parameters from stimulus
[tFrameDuration, tStimulusDuration] = DealParamsOrNaN(sStimulus.cPresentationParameters{:});
[mfGaborLocsDeg, fPixelsPerDeg, ...
   vfGaborWidthDeg, vfGaborSFCPD, vfGaborContrast, ...
   vfGaborOrientationRad, vfGaborPhaseRad, ...
   vfGaborDriftTFHz, vfGaborRotationHz] = ...
      DealParamsOrNaN(sStimulus.cStimulusArguments{:});
   
strFrameDuration = CheckNanSprintf('%.2f ms', [], tFrameDuration*1e3, tFrameDuration*1e3);
strStimulusDuration = CheckNanSprintf('%.2f s', [], tStimulusDuration, tStimulusDuration);

strGaborLocsDeg = CheckNanSprintf(['[' repmat('%.2f %.2f, ', 1, size(mfGaborLocsDeg, 1)) '] deg'], [], mfGaborLocsDeg, mfGaborLocsDeg);
strPixelsPerDeg = CheckNanSprintf('%.2f PPD', [], fPixelsPerDeg, fPixelsPerDeg);
strGaborWidthDeg = CheckNanSprintf(['[' repmat('%.2f , ', 1, numel(vfGaborWidthDeg)) '] deg'], [], vfGaborWidthDeg, vfGaborWidthDeg);
strGaborSFCPD = CheckNanSprintf(['[' repmat('%.2f , ', 1, numel(vfGaborSFCPD)) '] CPD'], [], vfGaborSFCPD, vfGaborSFCPD);
strGaborContrast = CheckEmptyNanSprintf(['[' repmat('%.2f , ', 1, numel(vfGaborContrast)) ']'], '(1)', [], vfGaborContrast, vfGaborContrast);
strGaborOrientationRad = CheckEmptyNanSprintf(['[' repmat('%.2f , ', 1, numel(vfGaborOrientationRad)) '] deg'], '(vertical)', [], vfGaborOrientationRad*180/pi, vfGaborOrientationRad*180/pi);
strGaborPhaseRad = CheckEmptyNanSprintf(['[' repmat('%.2f , ', 1, numel(vfGaborPhaseRad)) '] rad'], '(odd)', [], vfGaborPhaseRad, vfGaborPhaseRad);
strGaborDriftTFHz = CheckEmptyNanSprintf(['[' repmat('%.2f , ', 1, numel(vfGaborDriftTFHz)) '] Hz'], '(stationary)', [], vfGaborDriftTFHz, vfGaborDriftTFHz);
strGaborRotationHz = CheckEmptyNanSprintf(['[' repmat('%.2f , ', 1, numel(vfGaborRotationHz)) '] Hz'], '(stationary)', [], vfGaborRotationHz, vfGaborRotationHz);

   
% -- Produce description
   
strDescription = [strIndent sprintf('Arbitrary Gabor field stimulus\n') ...
                  strIndent sprintf('Parameter list:\n')];
strIndent = [strIndent '   '];

strDescription = [strDescription ...
   strIndent sprintf('Frame duration: %s\n', strFrameDuration) ...
   strIndent sprintf('Stimulus duration: %s\n', strStimulusDuration) ...
   strIndent sprintf('Gabor locations: %s\n', strGaborLocsDeg) ...
   strIndent sprintf('Screen calibration: %s\n', strPixelsPerDeg) ...
   strIndent sprintf('Gabor widths: %s\n', strGaborWidthDeg) ...
   strIndent sprintf('Gabor spatial freq.s: %s\n', strGaborSFCPD) ...
   strIndent sprintf('Gabor contrasts: %s\n', strGaborContrast) ...
   strIndent sprintf('Gabor orientations: %s\n', strGaborOrientationRad) ...
   strIndent sprintf('Gabor phases: %s\n', strGaborPhaseRad) ...
   strIndent sprintf('Gabor drit rates: %s\n', strGaborDriftTFHz) ...
   strIndent sprintf('Gabor rotation rates: %s\n', strGaborRotationHz)];

% --- END of DescribeGaborFieldStimulus FUNCTION ---


function strString = CheckNanSprintf(strFormat, strNanFormat, oCheckNan, varargin)

if (~isnan(oCheckNan))
   strString = sprintf(strFormat, varargin{:});

elseif isempty(strNanFormat)
   strString = '(dynamic)';

else
   strString = sprintf(strEmptyFormat, varargin{:});
end

function strString = CheckEmptyNanSprintf(strFormat, strEmptyFormat, strNanFormat, oCheckEmptyNan, varargin)

if (isnan(oCheckEmptyNan))
   if (isempty(strNanFormat))
      strString = '(dynamic)';
   else
      strString = sprintf(strNanFormat, varargin{:});
   end
   
elseif (isempty(oCheckEmptyNan))
   if isempty(strEmptyFormat)
      strString = '(dynamic)';
   else
      strString = sprintf(strEmptyFormat, varargin{:});
   end
   
else
   strString = sprintf(strFormat, varargin{:});
end

% --- END of DescribeGaborFieldStimulus.m ---
