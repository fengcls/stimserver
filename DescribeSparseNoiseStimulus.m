function [strDescription] = DescribeSparseNoiseStimulus(sStimulus, strIndent)

% SparseNoiseStimulus - STIMULUS A stimulus composed of single active regions, randomly placed on a field, to characterise a simple cell receptive field
%
% This stimulus is a uniform field, on which single active regions are placed
% randomly in turn.  A sequence consisting of all possible active region
% locations, with a random order, is generated.  The number of different
% contrasts (or colours) used for the active regions is fully configurable.
% 
% Presentation parameters:
%
%     tFrameDuration - The desired duration of a single frame
%     nNumRepeats - The number of times to repeat the stimulus presentation
%        (there for convenience) 
%
% Stimulus parameters:
%
%     vfUnitSizeDegrees - A two-element vector [fXSize fYSize], where 'fXSize' and
%        'fYSize' define the size of each of the active regions (or "units")
%        used in the stimulus.
%     vnNumUnits - A two-element vector [nXSize nYSize], specifying the number
%        of active units used in the stimulus, as a grid of 'nXSize' x 'nYSize'.
%     fPixelsPerDegree - The calibration of the presentation screen, in pixels
%        per degree.
%     vnSeed - A list (optionally a single number) of integers to use as seeds
%        for the pseudo-random sequence, so that the sequence is repeatable.  If
%        a list is provided, then the sequence will consist of a number of
%        sub-sequences equal to the length of this list, where each sub-sequence
%        in turn is the full sparse noise sequence corresponding to each of the
%        seeds in 'vnSeed'.
%     nNumFrames - An optional parameter specifying the total number of frames
%        to generate.  This number must be smaller or equal to the number of
%        frames in the full random sequence.  If provided, a sequence of length
%        'nNumFrames' will be returned, beginning from the first frame in the
%        random sequence.
%     vfPixelValues - A vector specifying the colour indices used to generate
%        the stimulus.  It has the format [nFieldIndex nActive1Index
%        nActive2Index ...]  'nFieldIndex' specifies the colour index of the
%        uniform background field.  The following indices specify the number and
%        colour indices of the active regions.  The default is [0.5 0 1],
%        meaning that black (0) and white (1) active regions are superimposed on
%        a grey (0.5) uniform field.  These values must be provided as indices
%        that PsychToolbox understands, using for example the BlackIndex and
%        WhiteIndex functions.

% DescribeSparseNoiseStimulus - FUNCTION Describe the parameters for an square-wave grating stimulus
%
% Usage: [strDescription] = DescribeSparseNoiseStimulus(sStimulus <, strIndent>)
%
% Usually called automatically by StimulusServerDescribeStimuli

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 1st October, 2010

% -- Check arguments

if (nargin == 0)
   SSlog('*** DescribeSparseNoiseStimulus: Incorrect usage\n');
   help DescribeSparseNoiseStimulus;
   return;
end

if (~exist('strIndent', 'var') || isempty(strIndent))
   strIndent = '';
end


% -- Convert parameters to parameter strings

% - Extract parameters from stimulus
[tFrameDuration, nNumRepeats] = DealParamsOrNaN(sStimulus.cPresentationParameters{:});
[vfUnitSizeDegrees, vnNumUnits, fPixelsPerDegree, ...
   vnSeed, nNumFrames, vfPixelValues] = ...
      DealParamsOrNaN(sStimulus.cStimulusArguments{:});

strFrameDuration = CheckNanSprintf('%.2f ms', [], tFrameDuration, tFrameDuration*1e3);
strNumRepeats = CheckNanSprintf('%d', [], nNumRepeats, nNumRepeats);
strUnitSizeDegrees = CheckNanSprintf('[%.2f x %.2f] deg', [], vfUnitSizeDegrees, vfUnitSizeDegrees);
strNumUnits = CheckNanSprintf('[%d x %d]', [], vnNumUnits, vnNumUnits);
strPixelsPerDegree = CheckNanSprintf('%.2f ppd', [], fPixelsPerDegree, fPixelsPerDegree);
strSeed = ['[' CheckEmptyNanSprintf(' %d', '1', [], vnSeed, vnSeed) ']'];
strNumFrames = CheckEmptyNanSprintf('%d', '(full sequence)', [], nNumFrames, nNumFrames);
strPixelValues = ['[' CheckEmptyNanSprintf(' %.2f', '(0.5 0 1)', '(dynamic or 0.5 0 1)', vfPixelValues, vfPixelValues) ']'];
   
% -- Produce description
   
strDescription = [strIndent sprintf('Oscillating plaid stimulus\n') ...
                  strIndent sprintf('Parameter list:\n')];
strIndent = [strIndent '   '];

strDescription = [strDescription ...
   strIndent sprintf('Frame duration: %s\n', strFrameDuration) ...
   strIndent sprintf('Number of repeats: %s\n', strNumRepeats) ...
   strIndent sprintf('Active unit size: %s\n', strUnitSizeDegrees) ...
   strIndent sprintf('Number of units: %s\n', strNumUnits) ...
   strIndent sprintf('Screen calibration: %s\n', strPixelsPerDegree) ...
   strIndent sprintf('Random seed(s): %s\n', strSeed) ...
   strIndent sprintf('Number of frames in sequence: %s\n', strNumFrames) ...
   strIndent sprintf('Active unit colour values: %s\n', strPixelValues)];
   
% --- END of DescribeSparseNoiseStimulus.m ---


function strString = CheckNanSprintf(strFormat, strEmptyFormat, oCheckNan, varargin)

if (~isnan(oCheckNan))
   strString = sprintf(strFormat, varargin{:});

elseif isempty(strEmptyFormat)
   strString = '(dynamic)';

else
   strString = sprintf(strEmptyFormat, varargin{:});
end


function strString = CheckEmptyNanSprintf(strFormat, strEmptyFormat, strNanFormat, oCheckEmptyNan, varargin)

if (isnan(oCheckEmptyNan))
   if (isempty(strNanFormat))
      strString = '(dynamic)';
   else
      strString = sprintf(strNanFormat, varargin{:});
   end
   
elseif (isempty(oCheckEmptyNan))
   if isempty(strEmptyFormat)
      strString = '(dynamic)';
   else
      strString = sprintf(strEmptyFormat, varargin{:});
   end
   
else
   strString = sprintf(strFormat, varargin{:});
end

% --- END of DescribeSparseNoiseStimulus.m ---
