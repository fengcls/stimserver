function [strDescription] = DescribeSquareGratingStimulus(sStimulus, strIndent)

% SquareGratingStimulus - STIMULUS A square-wave grating, that can be dynamically shifted and rotated
%
% This stimulus is a black and white square-wave grating, presented at some
% orientation.  The stimulus can be shifted during presentation, as well as
% rotated during presentation.
% 
% Presentation parameters:
%
%     tFrameDuration - The desired duration of a single frame
%     tStimulusDuration - The duration of the entire stimulus
%     nNumRepeats - The number of times to repeat the stimulus presentation
%        (there for convenience) 
%
% Stimulus parameters:
%
%     vfSizeDegrees - A two-element vector [fXSize fYSize], where 'fXSize' and
%        'fYSize' define the size of the stimulus presentation region in degrees.
%        If an empty matrix is provided, the the stimulus will fill the
%        presentation screen.
%     fCyclesPerDegree - The spatial frequency of the grating, in cycles per
%        degree.
%     fPixelsPerDegree - The calibration of the presentation screen, in pixels
%        per degree.
%     fBarWidthDegrees - The width of a single grating bar, in visual degrees.
%        If not provided, then the gratings have a 50% duty cycle.
%     fAngleDegrees - The orientation of the grating in degrees, where 0 degrees
%        is a vertical grating.
%     fShiftCyclesPerSec - The distance to shift the grating during the stimulus
%        presentation, in grating cycles per second.
%     fRotateCyclesPerSec - The amount to rotate the grating during the stimulus
%        presentation, in rotational cycles per second.
%     bDrawMask - 'true' if the stimulus should be masked. Default is
%     'false'.
%     bInvertMask - 'true' if the mask should be inverted. Default is
%     'false'.
%     fMaskDiameterDeg - The diameter size of the circular mask in degrees.
%     vfMaskPosPix - A two-element vector [fXPos fYPos], where 'fXPos' and
%     'fYPos' define the pixel position of the mask relative to the center
%     of the screen. Default is [0 0], the center of the screen.

% %                                          fContrast, bAdapt, ...
% %                                          tAdaptationDuration, fAdaptShiftPixPerFr, ...
% %                                          fAdaptContrast, vfCentreShiftPix)

% DescribeSquareGratingStimulus - FUNCTION Describe the parameters for an
% square-wave grating stimulus that can be presented with a circular mask
%
% Usage: [strDescription] = DescribeSquareGratingStimulus(sStimulus <, strIndent>)
%
% Usually called automatically by StimulusServerDescribeStimuli

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>, modified by Patricia Molina-Luna 
% to include masking parameters (24th January).
% Created: 1st October, 2010

% -- Check arguments

if (nargin == 0)
   SSlog('*** DescribeSquareGratingStimulus: Incorrect usage\n');
   help DescribeSquareGratingStimulus;
   return;
end

if (~exist('strIndent', 'var') || isempty(strIndent))
   strIndent = '';
end

% -- Convert parameters to parameter strings

% - Extract parameters from stimulus
[tFrameDuration, tStimulusDuration, nNumRepeats] = DealParamsOrNaN(sStimulus.cPresentationParameters{:});
[vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, ...
   fBarWidthDegrees, fAngleDegrees, fShiftCyclesPerSec, fRotateCyclesPerSec, ...
   bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskPosPix] = ...
      DealParamsOrNaN(sStimulus.cStimulusArguments{:});

strFrameDuration = CheckNanSprintf('%.2f ms', [], tFrameDuration, tFrameDuration*1e3);
strStimulusDuration = CheckNanSprintf('%.2f s', [], tStimulusDuration, tStimulusDuration);
strNumRepeats = CheckNanSprintf('%d', [], nNumRepeats, nNumRepeats);
strSizeDegrees = CheckEmptyNanSprintf('[%.2f x %.2f] deg', '(full screen)', [], vfSizeDegrees, vfSizeDegrees);
strCyclesPerDegree = CheckNanSprintf('%.2f cpd; %.2f dpc', [], fCyclesPerDegree, fCyclesPerDegree, 1/fCyclesPerDegree);
strPixelsPerDegree = CheckNanSprintf('%.2f ppd', [], fPixelsPerDegree, fPixelsPerDegree);
strBarWidthDegrees = CheckEmptyNanSprintf('%.2f deg', '(0.5 duty cycle)', [], fBarWidthDegrees, fBarWidthDegrees);
strAngleDegrees = CheckNanSprintf('%.2f deg', [], fAngleDegrees, fAngleDegrees);
strShiftCyclesPerSec = CheckEmptyNanSprintf('%.2f Hz', '(stationary)', '(dynamic or stationary)', fShiftCyclesPerSec, fShiftCyclesPerSec);
strRotateCyclesPerSec = CheckEmptyNanSprintf('%.2f Hz', '(stationary)', '(dynamic or stationary)', fRotateCyclesPerSec, fRotateCyclesPerSec);
strDrawMask = CheckNanSprintf('%d', [], bDrawMask, bDrawMask);
strInvertMask = CheckNanSprintf('%d', [], bInvertMask, bInvertMask);
strMaskDiamterDeg = CheckNanSprintf('%.2f deg', [], fMaskDiameterDeg, fMaskDiameterDeg);
strMaskPosPix = CheckNanSprintf('[%.2f x %.2f]', [],  vfMaskPosPix, vfMaskPosPix);

% -- Produce description
   
strDescription = [strIndent sprintf('Square wave grating stimulus\n') ...
                  strIndent sprintf('Parameter list:\n')];
strIndent = [strIndent '   '];

strDescription = [strDescription ...
   strIndent sprintf('Frame duration: %s\n', strFrameDuration) ...
   strIndent sprintf('Stimulus duration: %s\n', strStimulusDuration) ...
   strIndent sprintf('Number of repeats: %s\n', strNumRepeats) ...
   strIndent sprintf('Stimulus size: %s\n', strSizeDegrees) ...
   strIndent sprintf('Grating size: %s\n', strCyclesPerDegree) ...
   strIndent sprintf('Screen calibration: %s\n', strPixelsPerDegree) ...
   strIndent sprintf('Grating bar width: %s\n', strBarWidthDegrees) ...
   strIndent sprintf('Grating orientation: %s\n', strAngleDegrees) ...
   strIndent sprintf('Grating drift: %s\n', strShiftCyclesPerSec) ...
   strIndent sprintf('Grating rotation: %s\n', strRotateCyclesPerSec) ...
   strIndent sprintf('Grating Masked: %s\n', strDrawMask) ...
   strIndent sprintf('Mask inverted: %s\n', strInvertMask) ...
   strIndent sprintf('Mask diameter: %s\n', strMaskDiamterDeg) ...
   strIndent sprintf('Mask position: %s\n', strMaskPosPix)];
   
% --- END of DescribeSquareGratingStimulus.m ---

function strString = CheckNanSprintf(strFormat, strEmptyFormat, oCheckNan, varargin)

if (~isnan(oCheckNan))
   strString = sprintf(strFormat, varargin{:});

elseif isempty(strEmptyFormat)
   strString = '(dynamic)';

else
   strString = sprintf(strEmptyFormat, varargin{:});
end

function strString = CheckEmptyNanSprintf(strFormat, strEmptyFormat, strNanFormat, oCheckEmptyNan, varargin)

if (isnan(oCheckEmptyNan))
   if (isempty(strNanFormat))
      strString = '(dynamic)';
   else
      strString = sprintf(strNanFormat, varargin{:});
   end
   
elseif (isempty(oCheckEmptyNan))
   if isempty(strEmptyFormat)
      strString = '(dynamic)';
   else
      strString = sprintf(strEmptyFormat, varargin{:});
   end
   
else
   strString = sprintf(strFormat, varargin{:});
end

% --- END of DescribeSquareGratingStimulus.m ---
