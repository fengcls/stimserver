function [strDescription] = DescribeTextureStimulus(sStimulus, strIndent)

% TextureStimulus - STIMULUS A field of drifting bar segemnts
%
% This stimulus is a field of moving, oriented, semi-pseudorandomly
% distributed bar segments.
% 
% Presentation parameters:
%
%     tFrameDuration - The desired duration of a single frame
%     tStimulusDuration - The duration of the entire stimulus
%     nNumRepeats - The number of times to repeat the stimulus presentation
%        (there for convenience) 
% 
% Stimulus parameters:
%
%     vfSizeDegrees - A two-element vector [fXSize fYSize], where 'fXSize' and
%        'fYSize' define the size of the stimulus presentation region in degrees.
%        If an empty matrix is provided, the the stimulus will fill the
%        presentation screen.
%     fGridsPerDegree
%     fPixelsPerDegree - The calibration of the presentation screen, in pixels
%        per degree.
%     fDirectionDegrees
%     fOrientationDegrees
%     fShiftGridsPerSecond
%     fJitter
%     fWidthDegrees
%     fLengthDegrees
%     bGabor
%     fBandwidth
%     fPhaseoffset
%     nGridRepetition
%     fContrast
%     bAdapt
%     tAdaptationDuration
%     fAdaptGridsPerSec
%     fAdaptContrast
%     nSeed
%     vfGreyLevels       

% DescribeTextureStimulus - FUNCTION Describe the parameters for a
% field of pseudo-randomly distributed bar segments
%
% Usage: [strDescription] = DescribeTextureStimulus(sStimulus <, strIndent>)
%
% Usually called automatically by StimulusServerDescribeStimuli

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>, Andi Keller <andi@ini.phys.ethz.ch>
% Created: 1st October, 2010

% -- Check arguments

if (nargin == 0)
   SSlog('*** DescribeTextureStimulus: Incorrect usage\n');
   help DescribeTextureStimulus;
   return;
end

if (~exist('strIndent', 'var') || isempty(strIndent))
   strIndent = '';
end

% -- Convert parameters to parameter strings

% - Extract parameters from stimulus
[tFrameDuration, tStimulusDuration, nNumRepeats] = DealParamsOrNaN(sStimulus.cPresentationParameters{:});
[vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, ...
   fAngleDegrees, fShiftCyclesPerSec, fRotateCyclesPerSec, fContrast, ...
   bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskPosPix, tAdaptationDuration, ...
   bCyclicTF, fTFBandWidth, tTFRampDuration, tTFTransitionDuration] = ...
      DealParamsOrNaN(sStimulus.cStimulusArguments{:});

strFrameDuration = CheckNanSprintf('%.2f ms', [], tFrameDuration, tFrameDuration*1e3);
strStimulusDuration = CheckNanSprintf('%.2f s', [], tStimulusDuration, tStimulusDuration);
strNumRepeats = CheckNanSprintf('%d', [], nNumRepeats, nNumRepeats);
strSizeDegrees = CheckEmptyNanSprintf('[%.2f x %.2f] deg', '(full screen)', [], vfSizeDegrees, vfSizeDegrees);
strCyclesPerDegree = CheckNanSprintf('%.2f cpd; %.2f dpc', [], fCyclesPerDegree, fCyclesPerDegree, 1/fCyclesPerDegree);
strPixelsPerDegree = CheckNanSprintf('%.2f ppd', [], fPixelsPerDegree, fPixelsPerDegree);
strAngleDegrees = CheckNanSprintf('%.2f deg', [], fAngleDegrees, fAngleDegrees);
strShiftCyclesPerSec = CheckEmptyNanSprintf('%.2f Hz', '(stationary)', '(dynamic or stationary)', fShiftCyclesPerSec, fShiftCyclesPerSec);
strRotateCyclesPerSec = CheckEmptyNanSprintf('%.2f Hz', '(stationary)', '(dynamic or stationary)', fRotateCyclesPerSec, fRotateCyclesPerSec);
strContrast = CheckNanSprintf('%.2f %%', '(100%%)', fContrast, fContrast*100);
strDrawMask = CheckNanSprintf('%d', [], bDrawMask, bDrawMask);
strInvertMask = CheckNanSprintf('%d', [], bInvertMask, bInvertMask);
strMaskDiamterDeg = CheckNanSprintf('%.2f deg', [], fMaskDiameterDeg, fMaskDiameterDeg);
strMaskPosPix = CheckNanSprintf('[%.2f x %.2f]', [],  vfMaskPosPix, vfMaskPosPix);
strAdaptationDuration = CheckNanSprintf('%.2f s', [], tAdaptationDuration, tAdaptationDuration);
strCyclicTF = CheckNanSprintf('%d', [], bCyclicTF, bCyclicTF);
strTFBandWidth = CheckEmptyNanSprintf('%.2f Hz', [], [], fTFBandWidth, fTFBandWidth);
strTFRampDuration = CheckNanSprintf('%.2f s', [], tTFRampDuration, tTFRampDuration);
strTFTransitionDuration = CheckNanSprintf('%.2f s', [], tTFTransitionDuration, tTFTransitionDuration);


% -- Produce description
   
strDescription = [strIndent sprintf('Oscillating plaid stimulus\n') ...
                  strIndent sprintf('Parameter list:\n')];
strIndent = [strIndent '   '];

strDescription = [strDescription ...
   strIndent sprintf('Frame duration: %s\n', strFrameDuration) ...
   strIndent sprintf('Stimulus duration: %s\n', strStimulusDuration) ...
   strIndent sprintf('Number of repeats: %s\n', strNumRepeats) ...
   strIndent sprintf('Stimulus size: %s\n', strSizeDegrees) ...
   strIndent sprintf('Grating size: %s\n', strCyclesPerDegree) ...
   strIndent sprintf('Screen calibration: %s\n', strPixelsPerDegree) ...
   strIndent sprintf('Grating orientation: %s\n', strAngleDegrees) ...
   strIndent sprintf('Grating drift: %s\n', strShiftCyclesPerSec) ...
   strIndent sprintf('Grating rotation: %s\n', strRotateCyclesPerSec) ...
   strIndent sprintf('Grating Masked: %s\n', strDrawMask) ...
   strIndent sprintf('Mask inverted: %s\n', strInvertMask) ...
   strIndent sprintf('Mask diameter: %s\n', strMaskDiamterDeg) ...
   strIndent sprintf('Mask position: %s\n', strMaskPosPix) ...
   strIndent sprintf('Adaptation duration: %s\n', strAdaptationDuration) ...
   strIndent sprintf('Cyclic temporal frequency tuning: %s\n', strCyclicTF) ...
   strIndent sprintf('Temporal frequency band width: %s\n', strTFBandWidth) ...
   strIndent sprintf('Temporal frequency ramp duration: %s\n', strTFRampDuration) ...
   strIndent sprintf('Temporal frequency transition duration: %s\n', strTFTransitionDuration)];

% --- END of DescribeTextureStimulus.m ---

function strString = CheckNanSprintf(strFormat, strEmptyFormat, oCheckNan, varargin)

if (~isnan(oCheckNan))
   strString = sprintf(strFormat, varargin{:});

elseif isempty(strEmptyFormat)
   strString = '(dynamic)';

else
   strString = sprintf(strEmptyFormat, varargin{:});
end

function strString = CheckEmptyNanSprintf(strFormat, strEmptyFormat, strNanFormat, oCheckEmptyNan, varargin)

if (isnan(oCheckEmptyNan))
   if (isempty(strNanFormat))
      strString = '(dynamic)';
   else
      strString = sprintf(strNanFormat, varargin{:});
   end
   
elseif (isempty(oCheckEmptyNan))
   if isempty(strEmptyFormat)
      strString = '(dynamic)';
   else
      strString = sprintf(strEmptyFormat, varargin{:});
   end
   
else
   strString = sprintf(strFormat, varargin{:});
end

% --- END of DescribeTextureStimulus.m ---
