function [mfGrating, cmfLocationDeg] = GenerateSineGratingStimulus(vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree)

% GenerateSineGratingStimulus - Compute an upright sine grating for use as a texture
%
% Usage: [mfGrating, cmfLocationDeg] = GenerateSineGratingStimulus(vnSize, fCyclesPerPixel)
%        [mfGrating, cmfLocationDeg] = GenerateSineGratingStimulus(vfSizeDegrees, fCyclesPerDegree <, fPixelsPerDegree>)
%
% 'vnSize' is a two-elelment vector [N M], defining the size of the grating
% texture to be NxM (in degrees, if 'fPixelsPerDegree' is supplied.
% 'fCyclesPerPixel' or 'fCyclesPerDegree' and 'fPixelsPerDegree' define the
% spatial frequency of the grating.
%
% 'mfGrating' will be a matlab matrix containing a grating stimulus.  The black
% and white values will be 0 (black) and 1 (white).
%
% 'cmfLocationDeg' will be a cell array {mfRowLocDeg mfColLocDeg}.  Each element
% will contain a mesh with each element specifying the location of the
% corresponding element in 'mfGrating', relative to the centre of the texture.
%
% Make the grating big enough that you can shift it around on the screen without
% seeing the edges of the texture.

% Author: Andreas Keller, based on Dylan Muir's GenerateSquareGratingStimulus
% Created: 7th Oktober, 2011


% -- Defaults

DEF_fPixelsPerDegree = 1;


% -- Check arguments

if (nargin < 2)
   disp('*** GenerateSineGratingStimulus: Incorrect usage');
   help GenerateSineGratingStimulus;
   return;
end

if (~exist('fPixelsPerDegree', 'var') || isempty(fPixelsPerDegree))
   fPixelsPerDegree = DEF_fPixelsPerDegree;
end


vnSize = round(vfSizeDegrees * fPixelsPerDegree);


% -- Make the grating

vfGrating = zeros(1, vnSize(2));

fPixelsPerCyc = (fPixelsPerDegree / fCyclesPerDegree);

% - Fill cycles with a sine grating
vfGrating = ((sin(2*pi*(1:vnSize(2))/fPixelsPerCyc)) + 1) / 2;

% - Replicate base bar to make grating
mfGrating = repmat(vfGrating, vnSize(1), 1);

% - Should we construct location matrices?
if (nargout > 1)
   vfRow = linspace(-vnSize(1)/2, vnSize(1)/2, vnSize(1)) ./ fPixelsPerDegree;
   vfCol = linspace(-vnSize(2)/2, vnSize(2)/2, vnSize(2)) ./ fPixelsPerDegree;
   [cmfLocationDeg{1}, cmfLocationDeg{2}] = ndgrid(vfRow, vfCol);
end

% --- END of GenerateSineGrating.m ---
