function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentArbitraryStimulus(sStimulus, hWindow, vtBlankTime)

% PresentArbitraryStimulus - FUNCTION Present a stimulus using a stimulus structure
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%           PresentArbitraryStimulus(sStimulus, hWindow, vtBlankTime)
%
% 'sStimulus' has the structure
%    '.fhPresentationFunction' - function handle that will be used to present
%        the stimulus.  It must be called like ...(hWindow, vtBlankTime, ...
%                                                  <presentation params list>, ...
%                                                  <stimulus arguments list>);
%        It must return [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...()
%
%    '.cPresentationParameters' - A cell array of arbitrary presentation
%        parameters to be passed to 'fhPresentationFunction'.  The first
%        argument is usually 'tFrameDuration'.
%
%    '.cStimulusArguments' - A cell array of arbitrary stimulus parameters to be
%        passed to 'fhPresentationFunction'.
%
% 'vtBlankTime' is a one- or two-element vector.  'vtBlankTime(1)' is the
% desired blank period, in seconds, before the onset of this stimulus.
% 'vtBlankTime(2)' is the time stamp of the true "start" time for this stimulus
% (the start of the blank period), which can pre-date the calling of this
% function (for example, if the screen was blanked for one second before this
% function was called).  This can be used to constrain stimulus onset to a
% pre-calculated schedule.  If 'vtBlankTime(2)' is not provided, the function
% will internally create its own start timestamp, which can cause accretion of
% delays.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 31st August, 2010

% - Call stimulus presentation function
[tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
   sStimulus.fhPresentationFunction(hWindow, vtBlankTime, ...
                                    sStimulus.cPresentationParameters{:}, ...
                                    sStimulus.cStimulusArguments{:});

% --- END of PresentArbitraryStimulus.m ---
