function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentBlank(hWindow, vtBlankTime, tStimBlankTime, varargin) %#ok<VANUS>

% PresentBlank - FUNCTION Present a blank screen for a specified duration
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%             PresentBlank(hWindow, vtBlankTime, tStimBlankTime)

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 2010

% - Skip blank presentation for empty vtBlankTime
if (isempty(vtBlankTime))
   tLastPresentation = [];
   bBlewFrameRate = [];
   bBlewBlankTime = [];
   return;
end
         
% -- Blank screen
tStartTime = FrameMarkerFlip(hWindow, false);
        
if (numel(vtBlankTime) > 1)
   tFlipTime = vtBlankTime(2) + tStimBlankTime;% + vtBlankTime(1);
else
   tFlipTime = tStartTime + tStimBlankTime;% + vtBlankTime;
end

% -- Flip screen (wait for blank deadline)
[tLastPresentation, nul, nul, Missed] = Screen('Flip', hWindow, tFlipTime);

bBlewBlankTime = (Missed > 0);

bBlewFrameRate = false;

% --- END of PresentBlank.m ---
