function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentGaborGridStimulus(hWindow, vtBlankTime, ...
            	tFrameDuration, tStimDuration, vnGridDims, fGridSpacingDeg, fPixelsPerDeg, ...
               mfGaborWidthDeg, mfGaborSFCPD, mfGaborContrast, ...
               mfGaborOrientationRad, mfGaborPhaseRad, ...
               mfGaborDriftTFHz, mfGaborRotationHz)
                           
% PresentGaborGridStimulus - FUNCTION Present a regular field of drifting Gabors
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%           PresentGaborGridStimulus(hWindow, vtBlankTime, ...
%             	tFrameDuration, tStimDuration, vnGridDims, fGridSpacingDeg, fPixelsPerDeg, ...
%                mfGaborWidthDeg, mfGaborSFCPD, mfGaborContrast, ...
%                mfGaborOrientationRad, mfGaborPhaseRad, ...
%                mfGaborDriftTFHz, mfGaborRotationHz)

% Author: Dylan Muir <muir@hifo.uzh.ch>
% Created: 7th November, 2012

% -- Check parameters

if (~exist('mfGaborOrientationRad', 'var'))
   mfGaborOrientationRad = [];
end

if (~exist('mfGaborPhaseRad', 'var'))
   mfGaborPhaseRad = [];
end

if (~exist('mfGaborDriftTFHz', 'var'))
   mfGaborDriftTFHz = [];
end

if (~exist('mfGaborRotationHz', 'var'))
   mfGaborRotationHz = [];
end


% -- Determine where the Gabors will be presented

vfXCentres = fGridSpacingDeg .* linspace(-vnGridDims(1)/2, vnGridDims(1)/2, vnGridDims(1));
vfYCentres = fGridSpacingDeg .* linspace(-vnGridDims(2)/2, vnGridDims(2)/2, vnGridDims(2));
[mfXLoc, mfYLoc] = ndgrid(vfXCentres, vfYCentres);
mfGaborLocsDeg = [mfXLoc(:) mfYLoc(:)];


% -- Present the Gabors

[tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
   PresentGaborFieldStimulus(hWindow, vtBlankTime, ...
   tFrameDuration, tStimDuration, mfGaborLocsDeg, fPixelsPerDeg, ...
   mfGaborWidthDeg(:), mfGaborSFCPD(:), mfGaborContrast(:), ...
   mfGaborOrientationRad(:), mfGaborPhaseRad(:), ...
   mfGaborDriftTFHz(:), mfGaborRotationHz(:));



% --- END of PresentGaborGridStimulus.m ---
                              
