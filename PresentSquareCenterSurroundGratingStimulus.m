function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
   PresentSquareCenterSurroundGratingStimulus(  hWindow, vtBlankTime, ...
                                                tFrameDuration, vtStimulusDuration, nNumRepeats, ...
                                                vfPixelsPerDegree, vfCSCyclesPerDegree, ...
                                                cvfCSBarWidthDegrees, vfCSAngleDegrees, ...
                                                vfCSShiftCyclesPerSecond, vfCSRotateCyclesPerSecond, ...
                                                vfCenterPosOffsetDegrees, cvfCASRadiusDegrees)

% PresentSquareCenterSurroundGratingStimulus - FUNCTION Present a square
% grating stimulus with an annulus
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%    PresentSquareCenterSurroundGratingStimulus(  hWindow, vtBlankTime, ...
%                                                 tFrameDuration, vtStimulusDuration, nNumRepeats, ...
%                                                 fPixelsPerDegree, vfCSCyclesPerDegree, ...
%                                                 cvfCSBarWidthDegrees, vfCSAngleDegrees, ...
%                                                 vfCSShiftCyclesPerSecond, vfCSRotateCyclesPerSecond, ...
%                                                 vfCenterPosOffsetDegrees, cvfCASRadiusDegrees)
%
% Parameters:
%  'tFrameDuration' - The desired duration of each stimulus frame, in seconds.
%
%  'vtStimulusDuration' - The total duration of a single stimulus presentation,
%     in seconds.  If 'vtStimulation' is a vector, then for every second duration
%     entry the direction of drift will be reversed.  The total stimulus
%     duration will be the sum of all entries in 'vtStimulusDuration'.
%
%  'nNumRepeats' - The number of repeat stimulus presentations to make each time
%     this function is called.
%
%  'fPixelsPerDegree' - The calibration of the display monitor, in pixels per
%     degree.
%
%  'vfCSCyclesPerDegree' - The spatial frequency of both gratings, in cycles per
%     degree.  This parameter is a vector with the format
%     ['fCentreCyclesPerDegree' 'fSurroundCyclesPerDegree'].  If a scalar value
%     is supplied, it will be used for both gratings.
%
%  'cvfCSBarWidthDegrees' - The width of the white bar for both gratings, in
%     degrees.  This parameter is a cell array or vector with the format
%     {'fCentreBarWidthDegrees' 'fSurroundBarWidthDegrees'}, or
%     ['fCentreBarWidthDegrees' 'fSurroundBarWidthDegrees'].  If a scalar value
%     is supplied, it will be used for both gratings.  If either value is the
%     empty matrix [], a 50% duty cycle will be used for that grating.
%
%  'vfCSAngleDegrees' - The angle of both gratings, in degrees.  Zero degrees is
%     a vertical grating, drifting to the left.  The direction rotates clockwise
%     with increasing angle.  This argument is a vector with the format
%     ['fCentreAngleDegrees' 'fSurroundAngleDegrees'].  If a scalar value is 
%     supplied, it will be used for both gratings.
%
%  'vfCSShiftCyclesPerSecond' - The temporal frequency of grating movement, in
%     cycles per second.  This argument is a vector with the format
%     ['fCentreShiftCyclesPerSecond' 'fSurroundShiftCyclesPerSecond'].  If a
%     scalar value is supplied, it will be used for both gratings.
%
%  'vfCenterPosOffsetDegrees' - The location of the centre of the stimulus, as
%     an offset from the centre of the presentation window, in degrees.  If an
%     empty matrix is provided, the stimulus will be centred on the presentation
%     window.
%
%  'cvfCASRadiusDegrees' - The radii used to determine the stimulus, in degrees.
%     This argument is a cell vector with the format
%     {'fCentreDisplayRadiusDegrees' 'fAnnulusWidthDegrees'
%     'fSurroundOuterRadiusDegrees'}.  If 'fAnnulusWidthDegrees' is an empty
%     matrix, then no annulus will appear.  If 'fSurroundOuterRadiusDegrees' is
%     an empty matrix or is omitted, then the surround stimulus will fill the
%     presentation window.

% Author: Patricia Molina-Luna, based on Dylan Muir's PresentSquareGratingStimulus.m
% Created: 20th January, 2011

% -- Persistent arguments

persistent PSCSGS_sParams PSCSGS_vnTexIDs;


% -- Check arguments

if (nargin < 13)
   SSlog('*** PresentSquareCenterSurroundGratingStimulus: Incorrect usage.\n');
   help PresentSquareCenterSurroundGratingStimulus;
   return;
end

% - Get screen size
vnScreenRect = Screen('Rect', hWindow');

if (~exist('vtBlankTime', 'var') || isempty(vtBlankTime))
   tStartTime = 0;
   vtBlankTime = 0;
   bPresentStimulus = false;
else
   % -- Blank screen
   tStartTime = Screen('Flip', hWindow);
   
   bPresentStimulus = true;
end

if (numel(vtBlankTime) > 1)
   tFlipTime = vtBlankTime(2) + vtBlankTime(1);
else
   tFlipTime = tStartTime + vtBlankTime(1);
end


% -- Check arguments

if (isempty(cvfCSBarWidthDegrees))
   cvfCSBarWidthDegrees = {[] []};
   
else
   if (isscalar(cvfCSBarWidthDegrees))
      cvfCSBarWidthDegrees = repmat(cvfCSBarWidthDegrees, 1, 2);
   end
   
   if (~iscell(cvfCSBarWidthDegrees))
      cvfCSBarWidthDegrees = mat2cell(reshape(cvfCSBarWidthDegrees, 1, 2), 1, 2);
   end
end

if (isscalar(vfCSCyclesPerDegree))
   vfCSCyclesPerDegree = vfCSCyclesPerDegree * [1 1];
end

if (isscalar(vfCSAngleDegrees))
   vfCSAngleDegrees = vfCSAngleDegrees * [1 1];
end

if (~exist('vfCSShiftCyclesPerSecond', 'var') || isempty(vfCSShiftCyclesPerSecond))
   vfCSShiftCyclesPerSecond = 0;
end

if (isscalar(vfCSShiftCyclesPerSecond))
   vfCSShiftCyclesPerSecond = vfCSShiftCyclesPerSecond * [1 1];
end

if (~exist('vfCSRotateCyclesPerSecond', 'var') || isempty(vfCSRotateCyclesPerSecond))
   vfCSRotateCyclesPerSecond = 0;
end

if (isscalar(vfCSRotateCyclesPerSecond))
   vfCSRotateCyclesPerSecond = vfCSRotateCyclesPerSecond * [1 1];
end

if (isscalar(vfPixelsPerDegree))
   vfPixelsPerDegree = vfPixelsPerDegree * [1 1];
end

if (isempty(vfCenterPosOffsetDegrees))
   vfCenterPosOffsetDegrees = [0 0];
end

vtStimulusDuration = cumsum(vtStimulusDuration);
tTotalStimDuration = vtStimulusDuration(end);


% -- Do we need to re-generate the stimulus?

if (isempty(PSCSGS_sParams))
   bGenerateStim = true;

else
   % - Test each relevant parameter
   bGenerateStim = ~isequal(PSCSGS_sParams.vfCSCyclesPerDegree, vfCSCyclesPerDegree);
   bGenerateStim = bGenerateStim | ~isequal(PSCSGS_sParams.hWindow, hWindow);
   bGenerateStim = bGenerateStim | ~isequal(PSCSGS_sParams.vfPixelsPerDegree, vfPixelsPerDegree);
   bGenerateStim = bGenerateStim | ~isequal(PSCSGS_sParams.cvfCSBarWidthDegrees, cvfCSBarWidthDegrees);
   bGenerateStim = bGenerateStim | ~isequal(PSCSGS_sParams.cvfCASRadiusDegrees, cvfCASRadiusDegrees);
   bGenerateStim = bGenerateStim | ~isequal(PSCSGS_sParams.vfCenterPosOffsetDegrees, vfCenterPosOffsetDegrees);
   bGenerateStim = bGenerateStim | ~isfield(PSCSGS_sParams, 'tfCSGratings');
end


% -- Generate stimulus

if (bGenerateStim)
   SSlog('--- PresentCenterSurroundSquareGratingStimulus: Generating stimulus...\n');
   
   % - Record parameters
   PSCSGS_sParams = [];
   PSCSGS_sParams.vfCSCyclesPerDegree = vfCSCyclesPerDegree;
   PSCSGS_sParams.hWindow = hWindow;
   PSCSGS_sParams.vfPixelsPerDegree = vfPixelsPerDegree;
   PSCSGS_sParams.cvfCSBarWidthDegrees = cvfCSBarWidthDegrees;   
   PSCSGS_sParams.cvfCASRadiusDegrees = cvfCASRadiusDegrees;
   PSCSGS_sParams.vfCSPixelsPerCycle = mean(vfPixelsPerDegree) ./ vfCSCyclesPerDegree;
   PSCSGS_sParams.vfCenterPosOffsetDegrees = vfCenterPosOffsetDegrees;
   
   % - Fill screen, if no outer radius is supplied
   if ((numel(cvfCASRadiusDegrees) < 3) || isempty(cvfCASRadiusDegrees{3}))
      [vnScreenSize(1), vnScreenSize(2)] = Screen('WindowSize', hWindow);
      vfScreenSizeDegrees = vnScreenSize ./ vfPixelsPerDegree;
      vfMaxCornerDist = max(cat(3, vfScreenSizeDegrees/2 + vfCenterPosOffsetDegrees, vfScreenSizeDegrees - (vfScreenSizeDegrees/2 + vfCenterPosOffsetDegrees)), [], 3);
      cvfCASRadiusDegrees{3} = sqrt(sum((vfMaxCornerDist).^2));
   end
   
   vfFullStimSizeDegrees = cvfCASRadiusDegrees{3} * [2 2] + 2./max(vfCSCyclesPerDegree);
   
   % - Generate grating stimuli
   mfCGrating = GenerateSquareGratingStimulus(vfFullStimSizeDegrees, vfCSCyclesPerDegree(1), mean(vfPixelsPerDegree), cvfCSBarWidthDegrees{1});
   mfSGrating = GenerateSquareGratingStimulus(vfFullStimSizeDegrees, vfCSCyclesPerDegree(2), mean(vfPixelsPerDegree), cvfCSBarWidthDegrees{2});
   
   % - Convert to screen colour indices
   nBlack = BlackIndex(hWindow);
   nWhite = WhiteIndex(hWindow);
   nGrey = round((nWhite + nBlack)/2); %#ok<NASGU>

   mfCGrating(mfCGrating == 0) = nBlack;
   mfCGrating(mfCGrating == 1) = nWhite;
   mfSGrating(mfSGrating == 0) = nBlack;
   mfSGrating(mfSGrating == 1) = nWhite;
   
   PSCSGS_sParams.tfCSGratings = cat(3, mfCGrating, mfSGrating);
   
   % - Clear texture cache
   PSCSGS_vnTexIDs  = [];
end

% - Create textures, if necessary
if (isempty(PSCSGS_vnTexIDs))
   bGenerateTextures = true;
   
else
   % - Test to see if our texture exists
   vnTextures = Screen('Windows');
   
   if (~all(ismember(PSCSGS_vnTexIDs, vnTextures)))
      bGenerateTextures = true;
   else
      bGenerateTextures = false;
   end
end


% -- Determine position of stimulus

vnCenterRect = CenterRect([0 0 size(PSCSGS_sParams.tfCSGratings(:, :, 1))], vnScreenRect);
vfCenterOffsetPixels = vfCenterPosOffsetDegrees .* vfPixelsPerDegree;
vnCenterRect = vnCenterRect + repmat(round(vfCenterOffsetPixels), 1, 2);


% -- Generate textures, if required

if (bGenerateTextures)
   SSlog('--- PresentCenterSurroundSquareGratingStimulus: Generating textures...\n');
   nWhite = WhiteIndex(hWindow);
   nBlack = BlackIndex(hWindow);
   nGrey = round((nWhite + nBlack)/2);
   
   if (nGrey == nWhite)
      nGrey = (nWhite + nBlack)/2;
   end
   
   nInvisible = nBlack;
   nVisible = nWhite;
   
   % - Check that we can actually use a shader
   AssertGLSL;
   
   % - Generate a shader to use
   hGlsl = MakeTextureDrawShader(hWindow, 'SeparateAlphaChannel');
   

   % -- Generate the mask textures

   mbCentreMask = Ellipse( cvfCASRadiusDegrees{1} * vfPixelsPerDegree(1), ...
                           cvfCASRadiusDegrees{1} * vfPixelsPerDegree(2));

	if (isempty(cvfCASRadiusDegrees{2}) || cvfCASRadiusDegrees{2} == 0)
      mbSurroundInnerMask = false;
   else
      mbSurroundInnerMask = Ellipse((cvfCASRadiusDegrees{1} + cvfCASRadiusDegrees{2}) * vfPixelsPerDegree(1), ...
                                    (cvfCASRadiusDegrees{1} + cvfCASRadiusDegrees{2}) * vfPixelsPerDegree(2));
	end
   
   mbSurroundOuterMask = Ellipse(cvfCASRadiusDegrees{3} * vfPixelsPerDegree(1), ...
                                 cvfCASRadiusDegrees{3} * vfPixelsPerDegree(1));
      
   vnFullMaskSize = size(PSCSGS_sParams.tfCSGratings(:, :, 1));
   
   mnCentreMaskP = double(mbCentreMask);
   mnCentreMaskP(mbCentreMask) = nVisible;
   mnCentreMaskP(~mbCentreMask) = nInvisible;
   
   mnCentreMask = ones(vnFullMaskSize) * nInvisible;
   vnCenterMask = CenterRect([0 0 size(mnCentreMaskP)], [0 0 vnFullMaskSize]);
   mnCentreMask(vnCenterMask(1)+1:vnCenterMask(3), vnCenterMask(2)+1:vnCenterMask(4)) = mnCentreMaskP;

   mbSurroundInnerMaskResize = false(size(mbSurroundOuterMask));
   vnCenterMask = CenterRect([0 0 size(mbSurroundInnerMask)], [0 0 size(mbSurroundOuterMask)]);
   mbSurroundInnerMaskResize(vnCenterMask(1)+1:vnCenterMask(3), vnCenterMask(2)+1:vnCenterMask(4)) = mbSurroundInnerMask;
   
   mnSurroundMaskP = double(mbSurroundOuterMask);
   mnSurroundMaskP(mbSurroundOuterMask) = nVisible;
   mnSurroundMaskP(~mbSurroundOuterMask) = nInvisible;
   mnSurroundMaskP(mbSurroundInnerMaskResize) = nInvisible;

   mnSurroundMask = ones(vnFullMaskSize) * nInvisible;
   vnCenterMask = CenterRect([0 0 size(mnSurroundMaskP)], [0 0 vnFullMaskSize]);
   mnSurroundMask(vnCenterMask(1)+1:vnCenterMask(3), vnCenterMask(2)+1:vnCenterMask(4)) = mnSurroundMaskP;
   
   tnCTexture = PSCSGS_sParams.tfCSGratings(:, :, 1);
   tnCTexture(:, :, 2) = mnCentreMask;
   
   tnSTexture = PSCSGS_sParams.tfCSGratings(:, :, 2);
   tnSTexture(:, :, 2) = mnSurroundMask;
   
   PSCSGS_vnTexIDs(1) = Screen('MakeTexture', hWindow, tnCTexture, [], [], [], [], hGlsl);
   PSCSGS_vnTexIDs(2) = Screen('MakeTexture', hWindow, tnSTexture, [], [], [], [], hGlsl);
end


% -- Present the stimulus 

vfCSShiftPixelsPerFrame = vfCSShiftCyclesPerSecond ./ vfCSCyclesPerDegree .* mean(vfPixelsPerDegree) .* tFrameDuration;
vfCSRotateDegreesPerFrame = vfCSRotateCyclesPerSecond .* tFrameDuration .* 360;


% -- Present stimulus

if (~bPresentStimulus)
   tLastPresentation = [];
   bBlewFrameRate = [];
   bBlewBlankTime = [];
   return;
end   

bFirst = true;
bBlewFrameRate = false;

tEndTime = inf;

tNextFlipTime = tFlipTime;

for (nRepeat = 1:nNumRepeats) %#ok<FORPF>
   % - Reset stimulus params
   vfCSCurrShift = [0 0];
   vfCSCurrAngle = vfCSAngleDegrees;
   nStimSection = 1;
   
   while (tNextFlipTime <= tEndTime)
      % - Draw centre and surround textures onto the screen, shifted and rotated
      % - Enable alpha blending for typical drawing of masked textures
      Screen('BlendFunction', hWindow, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

      Screen('DrawTexture', hWindow, PSCSGS_vnTexIDs(2), [], vnCenterRect, vfCSCurrAngle(2), [], [], [], [], [], [0 mod(vfCSCurrShift(2), PSCSGS_sParams.vfCSPixelsPerCycle(2)) 0 0]);
      Screen('DrawTexture', hWindow, PSCSGS_vnTexIDs(1), [], vnCenterRect, vfCSCurrAngle(1), [], [], [], [], [], [0 mod(vfCSCurrShift(1), PSCSGS_sParams.vfCSPixelsPerCycle(1)) 0 0]);

      % - Flip window
      [tCurrTime, nul, nul, Missed] = FrameMarkerFlip(hWindow, bFirst, tNextFlipTime); %#ok<ASGLU>
      
      % - Initialise stimulation and check deadlines
      if (bFirst)
         bBlewBlankTime = Missed > 0;
         tStartTime = tCurrTime;
         tNextFlipTime = tCurrTime;
         tEndTime = tStartTime + tTotalStimDuration;
         bFirst = false;
      else
         bBlewFrameRate = bBlewFrameRate | (Missed > 0);
      end

      % - Determine next flip time
      tNextFlipTime = tNextFlipTime + tFrameDuration;
      
      if (tNextFlipTime > (tStartTime + vtStimulusDuration(nStimSection)))
         vfCSRotateDegreesPerFrame = -vfCSRotateDegreesPerFrame;
         vfCSShiftPixelsPerFrame = -vfCSShiftPixelsPerFrame;
         nStimSection = nStimSection + 1;
      end
      
      % - Update shift and rotation parameters
      vfCSCurrAngle = vfCSCurrAngle + vfCSRotateDegreesPerFrame;
      vfCSCurrShift = vfCSCurrShift + vfCSShiftPixelsPerFrame;
   end
end

% - Display a warning
if (bBlewBlankTime || bBlewFrameRate)
   SSlog('--- PresentSquareCenterSurroundGratingStimulus: The desired frame rate or blanking period was not met.\n');
end

% - Return the last stimulation time
tLastPresentation = Screen('Flip', hWindow, tEndTime);


% --- END of PresentSquareCenterSurroundGratingStimulus.m ---

