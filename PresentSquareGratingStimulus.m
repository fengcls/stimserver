
function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentSquareGratingStimulus(   hWindow, vtBlankTime, ...
                                            tFrameDuration, tStimulusDuration, nNumRepeats, ...
                                            vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, ...
                                            fBarWidthDegrees, fAngleDegrees, fShiftCyclesPerSec, ...
                                            fRotateCyclesPerSec, bDrawMask, bInvertMask, ...
                                            fMaskDiameterDeg, vfMaskPosPix, fContrast, bAdapt, ...
                                            tAdaptationDuration, fAdaptShiftCyclesPerSec, fAdaptContrast, ...
                                            vfCentreShiftPix)

% PresentSquareGratingStimulus - FUNCTION Present a square grating
% stimulus. The stimulus can be presented with a circular mask.
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%             PresentOscillatingGratingStimulus(  hWindow, vtBlankTime, ...
%                                                 tFrameDuration, tStimulusDuration, nNumRepeats, ...
%                                                 vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, ...
%                                                 <fBarWidthDegrees>, fAngleDegrees, <fShiftCyclesPerSec>, ...
%                                                 <fRotateCyclesPerSec, bDrawMask, bInvertMask>, ...
%                                                 <fMaskDiameterDeg, vfMaskPosPix, fContrast, bAdapt>, ...
%                                                 <tAdaptationDuration, fAdaptShiftCyclesPerSec>, ...
%                                                 <fAdaptContrast, vfCentreShiftPix>)
%
% 'vfSizeDegrees' is a vector [X Y] that defines the size of the entire
% stimulus, in degrees.  Leave as an empty vector to fill the presentation
% screen.
% 'vfMaskPosPix' is a vector [X Y] that defines the mask position relative
% to the center of the screen.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>, modified by Patricia
% Molina-Luna to add masking (24th January, 2011), modified by
% Andreas Keller to include contrast and adaptation parameters (2011-10-20)
% Created: 31st August, 2010 (from PresentOscillatingGratingStimulus.m)


% -- Persistent arguments

persistent PSGS_sParams PSGS_nTexID;


% -- Check arguments

if (nargin < 8)
   disp('*** PresentSquareGratingStimulus: Incorrect usage');
   help PresentSquareGratingStimulus;
   return;
end

if (~exist('vtBlankTime', 'var') || isempty(vtBlankTime))
   tStartTime = 0;
   vtBlankTime = 0;
   bPresentStimulus = false;
else
   % -- Blank screen
   tStartTime = Screen('Flip', hWindow);
   
   bPresentStimulus = true;
end

if (numel(vtBlankTime) == 1)
   vtBlankTime(2) = vtBlankTime(1);
   vtBlankTime(1) = tStartTime;
end


% -- Check arguments
if (~exist('fBarWidthDegrees', 'var'))
   fBarWidthDegrees = [];
end

if (~exist('fShiftCyclesPerSec', 'var') || isempty(fShiftCyclesPerSec))
    fShiftCyclesPerSec = 0;
end

if (~exist('fRotateCyclesPerSec', 'var') || isempty(fRotateCyclesPerSec))
    fRotateCyclesPerSec = 0;
end

if (~exist('bDrawMask', 'var'))
    bDrawMask = [];
end

if (~exist('bInvertMask', 'var'))
    bInvertMask = [];
end

if (~exist('fMaskDiameterDeg','var'))
    fMaskDiameterDeg = [];
end

if (~exist('vfMaskPosPix','var'))
    vfMaskPosPix = [];
end

if (~exist('fContrast','var'))
    fContrast = 1;
end

if (~exist('bAdapt','var'))
    bAdapt = false;
end

if (~exist('tAdaptationDuration','var'))
    tAdaptationDuration = 0;
end

if (~exist('fAdaptShiftCyclesPerSec','var'))
    fAdaptShiftCyclesPerSec = 0;
end

if (~exist('fAdaptContrast','var'))
    fAdaptContrast = 1;
end

if (~exist('vfCentreShiftPix', 'var'))
    vfCentreShiftPix = [0 0];
end


% - Do we need to re-generate the stimulus?

if (isempty(PSGS_sParams))
   bGenerateStim = true;

else
   % - Test each relevant parameter
   bGenerateStim = ~isequal(PSGS_sParams.fCyclesPerDegree, fCyclesPerDegree);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.hWindow, hWindow);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fPixelsPerDegree, fPixelsPerDegree);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fBarWidthDegrees, fBarWidthDegrees);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.vfSizeDegrees, vfSizeDegrees);
end


% -- Generate stimulus

if (bGenerateStim)
   SSlog('--- PresentSquareGratingStimulus: Generating stimulus...\n');
   
   % - Record parameters
   PSGS_sParams = [];
   PSGS_sParams.fCyclesPerDegree = fCyclesPerDegree;
   PSGS_sParams.hWindow = hWindow;
   PSGS_sParams.fPixelsPerDegree = fPixelsPerDegree;
   PSGS_sParams.fBarWidthDegrees = fBarWidthDegrees;   
   PSGS_sParams.vfSizeDegrees = vfSizeDegrees;
   PSGS_sParams.fPixelsPerCycle = fPixelsPerDegree / fCyclesPerDegree;
   PSGS_sParams.fContrast = fContrast;
   PSGS_sParams.bAdapt = bAdapt;
   PSGS_sParams.tAdaptationDuration = tAdaptationDuration;
   PSGS_sParams.fAdaptShiftCyclesPerSec = fAdaptShiftCyclesPerSec;
   PSGS_sParams.fAdaptContrast = fAdaptContrast;

   
   % - Determine how big the stimulus should be, so it can be rotated and
   %   shifted
   if (isempty(vfSizeDegrees))
      [vnScreenSize(1), vnScreenSize(2)] = Screen('WindowSize', hWindow);
      
      vfScreenSizeDegrees = vnScreenSize ./ fPixelsPerDegree;
      vfSizeDegrees = (sqrt(sum(vfScreenSizeDegrees.^2)) + 1/fCyclesPerDegree) * [1 1];
   end
   
   % - Generate stimulus
   mfGrating = GenerateSquareGratingStimulus(vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, fBarWidthDegrees);
   
   % - Convert to screen colour indices
   nBlack = BlackIndex(hWindow);
   nWhite = WhiteIndex(hWindow);
   nGrey = round((nWhite + nBlack)/2); %#ok<NASGU>

   mfGrating(mfGrating == 0) = nBlack;
   mfGrating(mfGrating == 0.5) = nGrey;
   mfGrating(mfGrating == 1) = nWhite;
   
   PSGS_sParams.mfGrating = mfGrating;
   
   % - Clear texture cache
   PSGS_nTexID = [];
end

% - Create textures, if necessary
if (isempty(PSGS_nTexID))
   bGenerateTexture = true;
else
   % - Test to see if our texture exists
   vnTextures = Screen('Windows');
   
   if (~any(vnTextures == PSGS_nTexID))
      bGenerateTexture = true;
   else
      bGenerateTexture = false;
   end
end

% - Generate a texture, if required
if (bGenerateTexture)
   SSlog('--- PresentSquareGratingStimulus: Generating texture...\n');
   
   % - Check that we can actually use a shader
   AssertGLSL;
   
   % - Generate a shader to use
   hGlsl = MakeTextureDrawShader(hWindow, 'SeparateAlphaChannel');
   
   % - Generate the texture
   PSGS_nTexID = GenerateTextures(PSGS_sParams.mfGrating, hWindow, hGlsl);
end


% -- Present the stimulus using PresentSimpleStimulus

if (~bPresentStimulus)
    tLastPresentation = [];
    bBlewBlankTime = [];
    bBlewFrameRate = [];
    return;
end

fShiftPixelsPerFrame = fShiftCyclesPerSec / fCyclesPerDegree * fPixelsPerDegree * tFrameDuration;
fAdaptShiftPixPerFr = fAdaptShiftCyclesPerSec / fCyclesPerDegree * fPixelsPerDegree * tFrameDuration;
fRotateDegreesPerFrame = fRotateCyclesPerSec * tFrameDuration * 360;

[tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
    PresentSimpleStimulus(hWindow, vtBlankTime, ...
        tFrameDuration, tStimulusDuration, nNumRepeats, ...
        PSGS_nTexID, fShiftPixelsPerFrame, fAngleDegrees, fRotateDegreesPerFrame, ...
        PSGS_sParams.fPixelsPerCycle, fPixelsPerDegree, bDrawMask, bInvertMask, ...
        fMaskDiameterDeg, vfMaskPosPix, fContrast, bAdapt, tAdaptationDuration, ...
        fAdaptShiftPixPerFr, fAdaptContrast, [], [], [], vfCentreShiftPix);

% --- END of PresentSquareGratingStimulus.m ---


