function [sStimulus] = STIM_FlashedImageSequence(varargin)

% STIM_FlashedImageSequence - Stimulus object
%
% Usage: [sStimulus] = STIM_FlashedImageSequence(tFlashDuration, nNumRepeats, ...
%                                                tfImageStack, strStackIdentifier, bInvert, vfDisplaySizeDeg, ...
%                                                vfCentreOffsetDeg, vfPixelsPerDegree, ...
%                                                bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskOffsetPix, ...
%                                                fContrast, vnOrdering)

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 21st December, 2012 (Happy end of the world)

%- Deal out parameters from argument list
[tFlashDuration, nNumRepeats, ...
   tfImageStack, strStackIdentifier, bInvert, vfDisplaySizeDeg, ...
   vfCentreOffsetDeg, vfPixelsPerDegree, ...
   bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskOffsetPix, ...
   fContrast, vnOrdering] = DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {tFlashDuration, nNumRepeats, tfImageStack, strStackIdentifier, bInvert, vfDisplaySizeDeg, vfCentreOffsetDeg, vfPixelsPerDegree, ...
   bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskOffsetPix, ...
   fContrast, vnOrdering};
cStimParams = {};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));
cStimParams = cStimParams(~cellfun(@isnan_empty, cStimParams));

% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentFlashedImageSequence, ...
               cPresentationParams, cStimParams, @calc_duration, ...
               []);


function vbIsNAN = isnan_empty(voThing)

vbIsEmpty = isempty(voThing);

vbIsNAN = false(size(vbIsEmpty));
vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));


function tStimDuration = calc_duration(s, b)

if (~isempty(s.StimulusArguments{5}))
   tStimDuration = s.PresentationParameters{2} * numel(s.StimulusArguments{5}) * (s.PresentationParameters{1} + b);
else
   tStimDuration = s.PresentationParameters{2} * size(s.PresentationParameters{3}, 3) * (s.PresentationParameters{1} + b);
end

% --- END of STIM_FlashedImageSequence.m ---
