function [sStimulus] = STIM_GaborGrid(varargin)

% STIM_GaborGrid - Stimulus object
%
% Usage: [sStimulus] = STIM_GaborGrid( tFrameDuration, tStimDuration, ...
%                       vnGridDims, fGridSpacingDeg, fPixelsPerDeg, ...
%                       mfGaborWidthDeg, mfGaborSFCPD, mfGaborContrast, ...
%                       mfGaborOrientationRad, mfGaborPhaseRad, ...
%                       mfGaborDriftTFHz, mfGaborRotationHz)

% Author: Dylan Muir <muir@hifo.uzh.ch>
% Created: 9th November, 2012

%- Deal out parameters from argument list
[tFrameDuration, tStimDuration, ...
   vnGridDims, fGridSpacingDeg, fPixelsPerDeg, ...
   mfGaborWidthDeg, mfGaborSFCPD, mfGaborContrast, ...
   mfGaborOrientationRad, mfGaborPhaseRad, ...
   mfGaborDriftTFHz, mfGaborRotationHz] = ...
      DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {tFrameDuration, tStimDuration};
cStimParams = {vnGridDims, fGridSpacingDeg, fPixelsPerDeg, ...
   mfGaborWidthDeg, mfGaborSFCPD, mfGaborContrast, ...
   mfGaborOrientationRad, mfGaborPhaseRad, ...
   mfGaborDriftTFHz, mfGaborRotationHz};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));
cStimParams = cStimParams(~cellfun(@isnan_empty, cStimParams));

% - Construct "get duration" function
fhDurationFunction = @(s, b)(s.cPresentationParameters{2}+b);

% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentGaborGridStimulus, ...
               cPresentationParams, cStimParams, fhDurationFunction, ...
               @DescribeGaborGridStimulus);

            
function vbIsNAN = isnan_empty(voThing)

vbIsEmpty = isempty(voThing);

vbIsNAN = false(size(vbIsEmpty));
vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));



% --- END of STIM_GaborGrid.m ---
