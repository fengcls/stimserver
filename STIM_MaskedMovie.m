function [sStimulus] = STIM_MaskedMovie(varargin)

% STIM_MaskedMovie - Stimulus object
%
% Usage: [sStimulus] = STIM_MaskedMovie(tFrameDuration, nNumRepeats, tStimulusDuration, ...
%                                       bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskPosPix, vfMovieSizeDeg, ...
%                                       vfPixelsPerDegree, ...
%                                       strMovieName, nFitToScreenMode, bCenterMovieOnMask)

% Author: Dylan Muir and Patricia Molina-Luna
% Created: 26th October, 2010

%- Deal out parameters from argument list
[tFrameDuration, nNumRepeats, tStimulusDuration, bDrawMask, bInvertMask, fMaskDiameterDeg, ...
   vfMaskPosPix, vfMovieSizeDeg, vfPixelsPerDegree, ...
   strMovieName, nFitToScreenMode, bCenterMovieOnMask] = DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {tFrameDuration, nNumRepeats, tStimulusDuration, bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskPosPix, vfMovieSizeDeg, vfPixelsPerDegree};
cStimParams = {strMovieName, nFitToScreenMode, bCenterMovieOnMask};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));
cStimParams = cStimParams(~cellfun(@isnan_empty, cStimParams));

% - Make "get duration" function
fhDurationFunction = @(s, b)(s.cPresentationParameters{2}*s.cPresentationParameters{3}+b);

% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentMaskedMovieStimulus, ...
               cPresentationParams, cStimParams, fhDurationFunction, ...
               @DescribeMaskedMovieStimulus);

            
function vbIsNAN = isnan_empty(voThing)

vbIsEmpty = isempty(voThing);

vbIsNAN = false(size(vbIsEmpty));
vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));



% --- END of STIM_MaskedMovie.m ---
