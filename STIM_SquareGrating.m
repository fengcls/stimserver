function [sStimulus] = STIM_SquareGrating(varargin)

% STIM_SquareGrating - Stimulus object
%
% Usage: [sStimulus] = STIM_SquareGrating( tFrameDuration, tStimulusDuration, nNumRepeats, ...
%                                          vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, ...
%                                          fBarWidthDegrees, fAngleDegrees, fShiftCyclesPerSec, ...
%                                          fRotateCyclesPerSecond, bDrawMask, bInvertMask, ...
%                                          fMaskDiameterDeg, vfMaskPosPix, fContrast, bAdapt, ...
%                                          tAdaptationDuration, fAdaptShiftCyclesPerSec, ...
%                                          fAdaptContrast, vfCentreShiftPix)

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>, modified by Patricia
% Molina-Luna to include masking parameters (24th January), modified by
% Andreas Keller to include contrast and adaptation parameters (2011-10-20)
% Created: 26th October, 2010

%- Deal out parameters from argument list
[tFrameDuration, tStimulusDuration, nNumRepeats, ...
   vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, ...
   fBarWidthDegrees, fAngleDegrees, fShiftCyclesPerSec,...
   fRotateCyclesPerSecond, bDrawMask, bInvertMask, ...
   fMaskDiameterDeg, vfMaskPosPix, fContrast, bAdapt, ...
   tAdaptationDuration, fAdaptShiftCyclesPerSec, ...
   fAdaptContrast, vfCentreShiftPix] = DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {tFrameDuration, tStimulusDuration, nNumRepeats};
cStimParams = {vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, fBarWidthDegrees, ...
               fAngleDegrees, fShiftCyclesPerSec, fRotateCyclesPerSecond,...
               bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskPosPix, ...
               fContrast, bAdapt, tAdaptationDuration, fAdaptShiftCyclesPerSec, ...
               fAdaptContrast, vfCentreShiftPix};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));
cStimParams = cStimParams(~cellfun(@isnan_empty, cStimParams));

% - Construct "get duration" function
if bAdapt == true %necessary because bAdapt can be Nan
    fhDurationFunction = @(s, b)(s.cPresentationParameters{2}+s.cStimulusArguments{14}+b);
else
    fhDurationFunction = @(s, b)(s.cPresentationParameters{2}+b);
end

% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentSquareGratingStimulus, ...
               cPresentationParams, cStimParams, fhDurationFunction, ...
               @DescribeSquareGratingStimulus);

            
function vbIsNAN = isnan_empty(voThing)

vbIsEmpty = isempty(voThing);

vbIsNAN = false(size(vbIsEmpty));
vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));



% --- END of STIM_SquareGrating.m ---
