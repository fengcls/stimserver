function [sStimulus] = STIM_SquarePlaid(varargin)

% STIM_SinePlaid - Stimulus object
%
% Usage: [sStimulus] = STIM_SquarePlaid(tFrameDuration, tStimulusDuration, nNumRepeats, ...
%                                       vfSizeDegrees, vfCyclesPerDegree, fPixelsPerDegree, ...
%                                       vfAngleDegrees, vfShiftCyclesPerSec, vfRotateCyclesPerSec, ...
%                                       vfContrast)
% 
% Author: Dylan Muir <muir@hifo.uzh.ch>
% Created: 10th Janurary, 2012


%- Deal out parameters from argument list
[tFrameDuration, tStimulusDuration, nNumRepeats, ...
   vfSizeDegrees, vfCyclesPerDegree, fPixelsPerDegree, ...
   vfAngleDegrees, vfShiftCyclesPerSec, vfRotateCyclesPerSec, ...
   vfContrast] = DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {tFrameDuration, tStimulusDuration, nNumRepeats};
cStimParams = {vfSizeDegrees, vfCyclesPerDegree, fPixelsPerDegree, ...
               vfAngleDegrees, vfShiftCyclesPerSec, vfRotateCyclesPerSec, ...
               vfContrast};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));
cStimParams = cStimParams(~cellfun(@isnan_empty, cStimParams));

% - Construct "get duration" function
fhDurationFunction = @(s, b)(s.cPresentationParameters{2}+b);

% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentSquarePlaidStimulus, ...
               cPresentationParams, cStimParams, fhDurationFunction, ...
               @DescribeSquarePlaidStimulus);

            
function vbIsNAN = isnan_empty(voThing)

vbIsEmpty = isempty(voThing);

vbIsNAN = false(size(vbIsEmpty));
vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));



% --- END of STIM_SquarePlaid.m ---
