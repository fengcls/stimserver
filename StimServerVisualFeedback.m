function StimServerVisualFeedback(hWindow, strText, bError)

% StimServerVisualFeedback - FUNCTION Display some textual feedback on the screen
%
% Usage: StimServerVisualFeedback(hWindow, strText, bError)
%

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 31st January, 2011

% -- Check arguments

if (nargin < 2)
   SSlog('*** StimServerVisualFeedback: Incorrect usage.\n');
   help StimServerVisualFeedback;
   return;
end

if (exist('bError', 'var') && bError)
   vnColour = [255 128 128 255];
else
   vnColour = [255 255 255 255];
end


% -- Draw text

if (Screen('WindowKind', hWindow))
   Screen('TextFont', hWindow, 'Helvetica');
   Screen('TextSize', hWindow, 32);
   Screen('TextStyle', hWindow, 1);
   Screen('DrawText', hWindow, strText, 0, 0, vnColour, vnColour);
   Screen('Flip', hWindow);
end

% -- END of StimServerVisualFeedback.m ---
