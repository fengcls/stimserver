function StimulusServerDescribeStimuli

% StimulusServerDescribeStimuli - FUNCTION Send a description of the currently configured stimuli to the logging interface
%
% Usage: StimulusServerDescribeStimuli

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 22ndSeptember, 2010

% -- Global state

global STIMSERV_sStimulusList;


% -- Check stimuli

if (isempty(STIMSERV_sStimulusList))
   SSlog('*** StimulusServerDescribeStimuli: No stimuli configured\n');
   return;
end


% -- Describe stimuli iteratively

% - Initialise description
strDescription = 'Stimuli descriptions:\n';
strCurrentIndent = '   ';

for (nStimID = 1:numel(STIMSERV_sStimulusList))
   strDescription = [strDescription strCurrentIndent ...
      sprintf('Stimulus ID [%d]:\n', nStimID')]; %#ok<AGROW>
   
   strDescription = [strDescription ...
      DescribeGenericStimulus(STIMSERV_sStimulusList(nStimID), [strCurrentIndent '   ']) '\n']; %#ok<AGROW>
end

SSlog(strDescription);


function strDescription = DescribeGenericStimulus(sStimulus, strIndent)

if (isequal(sStimulus.fhPresentationFunction, @PresentStimulusSequence))
   strDescription = DescribeStimulusSequence(sStimulus, strIndent);
else
   
   if (isfield(sStimulus, 'fhDescriptionFunction') && ~isempty(sStimulus.fhDescriptionFunction))
      strDescription = sStimulus.fhDescriptionFunction(sStimulus, strIndent);
   else
      
      strDescription = [strIndent ...
         sprintf('Presentation function: %s\n', func2str(sStimulus.fhPresentationFunction))];
      
      strDescription = [strDescription strIndent ...
         sprintf('Presentation Parameters: %d fixed parameter(s)\n', numel(sStimulus.cPresentationParameters))];
      
      strDescription = [strDescription strIndent ...
         sprintf('Stimulus Arguments: %d fixed parameter(s)\n', numel(sStimulus.cStimulusArguments))];
   end
end


function strDescription = DescribeStimulusSequence(sSequence, strIndent)

vsStimuli = sSequence.cStimulusArguments{1};
nNumStimuli = numel(vsStimuli);

strDescription = [strIndent sprintf('SEQUENCE with [%d] elements\n', nNumStimuli)];

if (isempty(sSequence.cPresentationParameters))
    strOrder = ['(' num2str(1:numel(sSequence.cStimulusArguments{1})) ')'];
else
    strOrder = num2str(sSequence.cPresentationParameters{1});
end

strDescription = [strDescription strIndent ...
    sprintf('Presentation order: %s\n', strOrder)];

strIndent = [strIndent '|  '];

for (nSeqID = 1:nNumStimuli)
   strDescription = [strDescription strIndent ...
      sprintf('Sequence ID [%d]:\n', nSeqID)]; %#ok<AGROW>
   
   strDescription = [strDescription ...
      DescribeGenericStimulus(vsStimuli(nSeqID), strIndent) ...
      strIndent '\n'];    %#ok<AGROW>
end



% --- END of StimulusServerDescribeStimuli.m ---
