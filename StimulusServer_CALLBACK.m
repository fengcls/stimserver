function StimulusServer_CALLBACK(hUDP, hEvent) %#ok<INUSD>

% StimulusServer_CALLBACK - CALLBACK Core stimulus server callback for Instument Control toolbox
%
% Do not call this function.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 31st August, 2010

% -- Global state

global STIMSERV_sStimulusList STIMSERV_hUDP STIMSERV_sState; %#ok<NUSED>


% -- Check arguments

if (nargin < 2)
   SSlog('*** StimulusServer_CALLBACK: Do not call this function directly.\n');
   return;
end


% -- Check state of UDP object

if (~isequal(get(STIMSERV_hUDP, 'Status'), 'open'))
   SSlog('*** StimulusServer: The server has not been started.\n');
   SSlog('       Start the server with the function ''StartStimulusServer''.\n');
   return;
end

% -- Loop while commands are available

while(get(STIMSERV_hUDP, 'BytesAvailable') > 0)
   % -- Read and decode the stimulus command
   % - Read the available command
   strCommand = fscanf(STIMSERV_hUDP);
   
   % - Pass the command to the command decoder
   StimulusServer_COMMAND(strCommand);
end

% - Send the IDLE state
SStalkback('SST IDLE');


% --- END of StimulusServer_CALLBACK.m ---
