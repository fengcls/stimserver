--- TO DO list for Stimulus server

Movies:
* Perform buffering for long movies, rather than loading entire movie

Stimulus presentation:
* Modify SS PRESENT command to accept parameters something similar to matlab's
     method of defining figure and plot parameters


--- Done!

Intrinsic imaging:
Set up TCP trigger for StimServer

Sequences:
* STIM_Sequence function

Lava lamp:
* Generate stimulus to disk rather than to memory

Movies:
* Pre-load / buffer more than one movie
